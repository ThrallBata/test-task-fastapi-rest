FROM python:3.11

RUN mkdir /fastapi_app

WORKDIR /fastapi_app

COPY . .

RUN pip install poetry

RUN poetry install

RUN chmod a+x docker/*.sh
